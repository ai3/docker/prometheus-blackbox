FROM registry.git.autistici.org/pipelines/images/base/debian:bookworm

RUN apt-get -q update && \
    echo prometheus-blackbox-exporter prometheus-blackbox-exporter/want_cap_net_raw boolean true \
       | debconf-set-selections && \
    env DEBIAN_FRONTEND=noninteractive apt-get -y install prometheus-blackbox-exporter ca-certificates && \
    apt-get clean && \
    rm -fr /var/lib/apt/lists/*

ENTRYPOINT ["/usr/bin/prometheus-blackbox-exporter"]

